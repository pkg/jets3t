#!/bin/sh 

set -e

# called by uscan with '--upstream-version' <version> <file>
echo "version $2"
package=`dpkg-parsechangelog | sed -n 's/^Source: //p'`
debian_version=`dpkg-parsechangelog | sed -ne 's/^Version: \(.*+dfsg\)-.*/\1/p'`
TAR=${package}_${debian_version}.orig.tar.gz
DIR=${package}-${debian_version}.orig

# clean up the upstream sources
unzip $3 && mv ${package}-$2 $DIR
unzip $DIR/src.zip -d $DIR
GZIP=--best tar --numeric --group 0 --owner 0 --anchored \
   -X debian/orig-tar.excludes -c -v -z -f $TAR $DIR

rm -rf $3 $DIR
