=head1 NAME

jets3t-uploader - provide upload access to clients without Amazon S3 accounts

=head1 SYNOPSIS

jets3t-uploader [<property1Name>=<property1Value> .. <propertyNName>=<propertyNValue>]

=head1 OVERVIEW

A graphical application that Service Providers with S3 accounts may provide to
clients or customers without S3 accounts. Uploader allows users to upload files
to S3 using a simple wizard-based workflow, but all uploads must first be
authorized by a Gatekeeper service.

=head1 FILES

F</etc/jets3t/uploader.properties>

=head1 SEE ALSO

L<jets3t-synchronize(1)>,
http://jets3t.s3.amazonaws.com/applications/uploader.html
